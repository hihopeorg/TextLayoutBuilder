package com.facebook.fbui.textlayoutbuilder;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Text;
import ohos.agp.text.Layout;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.facebook.fbui.textlayoutbuilder", actualBundleName);
    }
    private  Layout mLayout;
    @Test
    public  void testSetLayout() {
        mLayout=new TextLayoutBuilder().setText("txt").build();
        Assert.assertTrue(mLayout!=null);
    }
    @Test
    public void testSetTextWidith() {
        mLayout=new TextLayoutBuilder().setText("text").setWidth(43).build();
        Assert.assertTrue(mLayout.getWidth()==43);
    }
    @Test
    public void testsetTextLineHeight() {
        mLayout=new TextLayoutBuilder().setText("text").setLineHeight(5).build();
        Assert.assertNotNull(mLayout.getHeight());
    }

    @Test
    public void testsetTextsetMaxWidth() {
        mLayout=new TextLayoutBuilder().setText("text").setMaxWidth(10).build();
        Assert.assertNotNull(mLayout.getLimitWidth(10));
    }
    @Test
    public void testsetAlignment() {
        mLayout=new TextLayoutBuilder().setText("text").setAlignment(43).build();
        Assert.assertNotNull(mLayout.getAscent(43));
    }

    @Test
    public void tesEllipsize() {
        mLayout=new TextLayoutBuilder().setText("text").setEllipsize(Text.TruncationMode.AUTO_SCROLLING).build();
        Assert.assertTrue(!(mLayout.getDescent(43)==43));
    }
    @Test
    public void testMinWidth() {
        mLayout=new TextLayoutBuilder().setText("text").setMinWidth(20).build();
        Assert.assertNotNull(mLayout.getLimitWidth(20));
    }
}