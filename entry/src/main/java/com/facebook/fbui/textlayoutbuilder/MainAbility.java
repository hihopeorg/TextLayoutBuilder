/*
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fbui.textlayoutbuilder;


import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.text.Font;
import ohos.agp.text.Layout;
import ohos.agp.utils.Color;

public class MainAbility extends Ability {
    private DirectionalLayout directionalLayout;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        directionalLayout= (DirectionalLayout) findComponentById(ResourceTable.Id_parent);
        setTextFont();
        setTextColor();
        setTextSize();
    }



    private void addSample(ILayout iLayout){

        Layout layout=iLayout.layout();
        if (layout!=null){
            JSampleViews jSampleViews=new JSampleViews(this,layout);
            jSampleViews.setLayoutConfig(new DirectionalLayout.LayoutConfig(layout.getWidth(),layout.getHeight()));
            jSampleViews.addDrawTask(jSampleViews);
            directionalLayout.addComponent(jSampleViews);
        }
    }

    private void setTextColor() {
        addSample(new LayoutImplColor());
        addSample(new LayoutImplColorRed());
        addSample(new LayoutImplColorBlue());

    }

    class LayoutImplColor implements ILayout{
        @Override
        public Layout layout() {
            return new TextLayoutBuilder()
                    .setText("字体颜色设置")
                    .setTextColor(new Color(Color.rgb(0,0,0)))
                    .setTextSize(80)
                    .setWidth(30)
                    .build();
        }
    }
    class LayoutImplColorRed implements ILayout{
        @Override
        public Layout layout() {
            return new TextLayoutBuilder()
                    .setText("Hello World Hello World Hello World Hello World ")
                    .setTextColor(new Color(Color.rgb(255,0,0)))
                    .setTextSize(80)
                    .build();
        }
    }

    class LayoutImplColorBlue implements ILayout{
        @Override
        public Layout layout() {
            return new TextLayoutBuilder()
                    .setText("Hello World Hello World Hello World Hello World ")
                    .setTextColor(new Color(Color.rgb(0,0,255)))
                    .setTextSize(80)
                    .build();
        }
    }


    private void setTextFont() {
        addSample(new LayoutImplFontDEFAULT());
        addSample(new LayoutImplFontDEFAULT_BOLD());
        addSample(new LayoutImplFontMONOSPACE());
        addSample(new LayoutImplFontSANS_SERIF());
        addSample(new LayoutImplFontSERIF());
    }



    class LayoutImplFontDEFAULT implements ILayout{
        @Override
        public Layout layout() {
            return new TextLayoutBuilder()
                    .setText("设置字体功能：")
                    .setTypeface(Font.DEFAULT)
                    .setTextSize(80)
                    .build();
        }
    }

    class LayoutImplFontDEFAULT_BOLD implements ILayout{
        @Override
        public Layout layout() {
            return new TextLayoutBuilder()
                    .setText("Hello World Hello World Hello World Hello World ")
                    .setTypeface(Font.DEFAULT_BOLD)
                    .setTextSize(80)
                    .build();
        }
    }
    class LayoutImplFontMONOSPACE implements ILayout{
        @Override
        public Layout layout() {
            return new TextLayoutBuilder()
                    .setText("Hello World Hello World Hello World Hello World ")
                    .setTypeface(Font.MONOSPACE)
                    .setTextSize(80)
                    .build();
        }
    }

    class LayoutImplFontSANS_SERIF implements ILayout{
        @Override
        public Layout layout() {
            return new TextLayoutBuilder()
                    .setText("Hello World Hello World Hello World Hello World ")
                    .setTypeface(Font.SANS_SERIF)
                    .setTextSize(80)
                    .build();
        }
    }

    class LayoutImplFontSERIF implements ILayout{
        @Override
        public Layout layout() {
            return new TextLayoutBuilder()
                    .setText("Hello World Hello World Hello World Hello World ")
                    .setTypeface(Font.SERIF)
                    .setTextSize(80)
                    .build();
        }
    }


    private void setTextSize() {
        addSample(new LayoutImpSize());
        addSample(new LayoutImpSize100());
        addSample(new LayoutImpSize60());

    }


    class LayoutImpSize implements ILayout{
        @Override
        public Layout layout() {
            return new TextLayoutBuilder()
                    .setText("文字大小设置:")
                    .setTextSize(80)
                    .build();
        }
    }
    class LayoutImpSize100 implements ILayout{
        @Override
        public Layout layout() {
            return new TextLayoutBuilder()
                    .setText("Hello World Hello World Hello World Hello World")
                    .setTextSize(100)
                    .build();
        }
    }


    class LayoutImpSize60 implements ILayout{
        @Override
        public Layout layout() {
            return new TextLayoutBuilder()
                    .setText("Hello World Hello World Hello World Hello World")
                    .setTextSize(60)
                    .build();
        }
    }




    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }



}
