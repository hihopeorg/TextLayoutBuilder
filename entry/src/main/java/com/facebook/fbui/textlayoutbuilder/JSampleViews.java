/*
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fbui.textlayoutbuilder;

import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Layout;
import ohos.app.Context;

public class JSampleViews extends Component implements Component.DrawTask {
    private Paint mPaint;
    private Layout layout;
    public JSampleViews(Context context, Layout layout){
        super(context);
        this.layout=layout;
        addDrawTask(this);
        mPaint=new Paint();
        mPaint.setAntiAlias(true);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        layout.drawText(canvas);
    }
}