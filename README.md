# TextLayoutBuilder

**本项目是基于开源项目TextLayoutBuilder进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/facebook/TextLayoutBuilder
）追踪到原项目版本**

#### 项目介绍

- 项目名称：TextLayoutBuilder
- 所属系列：ohos的第三方组件适配移植
- 功能：支持文本的创建、文本字体、大小、颜色设置等等
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/facebook/TextLayoutBuilder
- 原项目基线版本:v1.5.0,shal:96b282775b13d9b31d397d03897f9670389eadfd
- 编程语言：Java
- 外部库依赖：无

## 效果展示
![](textLayoutBuilder.gif)

#### 安装教程
方法一.
1. 编译TextLayoutBuilder的har包library.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       ……
   }
   ```
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法二.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.facebook.fbui.textlayoutbuilder.ohos:textlayoutbuilder:1.0.2'
}
```
#### 使用说明

1. 实现 ILayout接口
```

class LayoutImplColor implements ILayout{
    @Override
    public Layout layout() {
        return new TextLayoutBuilder()
                .setText("Hello World")
                .setTextColor(new Color(Color.rgb(0,0,0)))
                .setTextSize(80)
                .build();
    }
}
```

2. 将实现后的类添加到addSample方法中
```
addSample(LayoutImplColor)
```


#### 版本迭代

- v1.0.2

#### 版权和许可信息

Apache license version 2.0
