/*
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facebook.fbui.textlayoutbuilder;


import com.facebook.fbui.textlayoutbuilder.util.BoringLayout;
import ohos.agp.components.TableLayout;
import ohos.agp.components.Text;
import ohos.agp.render.BlurDrawLooper;
import ohos.agp.render.Paint;
import ohos.agp.render.render3d.components.LightComponent;
import ohos.agp.text.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.system.version.SystemVersion;
import ohos.utils.LruBuffer;

import java.util.Arrays;

/**
 * An utility class to create text {@link Layout}s easily.
 *
 * <p>This class uses a Builder pattern to allow re-using the same object to create text {@link
 * Layout}s with similar properties.
 */
public class TextLayoutBuilder {

    public @interface MeasureMode {
    }

    public static final int MEASURE_MODE_UNSPECIFIED = 0;
    public static final int MEASURE_MODE_EXACTLY = 1;
    public static final int MEASURE_MODE_AT_MOST = 2;

    public static final int DEFAULT_MAX_LINES = Integer.MAX_VALUE;

    private static final float DEFAULT_SPACING_ADD = 0.0f;
    private static final float DEFAULT_SPACING_MULT = 1.0f;
    private static final float DEFAULT_LINE_HEIGHT = Float.MAX_VALUE;

    private static final int EMS = 1;
    private static final int PIXELS = 2;
    private TextAlignment mTextDir;
    private int mMinWidth = 0;
    private int mMinWidthMode = PIXELS;
    private int mMaxWidth = Integer.MAX_VALUE;
    private int mMaxWidthMode = PIXELS;

    // Cache for text layouts.

    private LruBuffer<Integer, Layout> sCache ;

    /**
     * Params for creating the layout.
     */

    Context context;
    static class Params {
        Paint paint = new Paint();
        float mShadowDx;
        float mShadowDy;
        float mShadowRadius;
        int mShadowColor;
        int width;

        LightComponent mLightComponent;
        Color color;
        int measureMode;
        float spacingMult = DEFAULT_SPACING_MULT;
        float spacingAdd = DEFAULT_SPACING_ADD;
        float lineHeight = DEFAULT_LINE_HEIGHT;
        boolean includePadding = true;
        boolean useLineSpacingFromFallbacks = SystemVersion.getApiVersion() >= 28;
        boolean shouldLayoutZeroLengthText = false;
        boolean isRtl = false;


        Text.TruncationMode ellipsize = null;
        boolean singleLine = false;
        int maxLines = DEFAULT_MAX_LINES;
        int alignment = LayoutAlignment.RIGHT;
        int textDirection =TextAlignment.RIGHT;
        int breakStrategy = 0;
        int hyphenationFrequency = 0;
        int justificationMode = 0; // JUSTIFICATION_MODE_NONE
        int[] leftIndents;
        int[] rightIndents;

        String text;
        boolean mForceNewPaint = false;

        /**
         * Create a new paint after the builder builds for the first time.
         */
        void createNewPaintIfNeeded() {
            if (mForceNewPaint) {
                Paint newPaint = new Paint(paint);
                newPaint.setAntiAlias(true);
                newPaint.set(paint);
                paint = newPaint;
                paint.setColor(Color.BLUE);
                mForceNewPaint = false;
            }


        }

        int getLineHeight() {
            return Math.round(paint.getFontMetrics().bottom * spacingMult + spacingAdd);//高度
        }

        @Override
        public int hashCode() {
            int hashCode = 1;
            hashCode = 31 * hashCode + Float.floatToIntBits(paint.getTextSize());
            hashCode = 31 * hashCode + (paint.getFontMetrics() != null ? paint.getFontMetrics().hashCode() : 0);
            hashCode = 31 * hashCode + Float.floatToIntBits(mShadowDx);
            hashCode = 31 * hashCode + Float.floatToIntBits(mShadowDy);
            hashCode = 31 * hashCode + Float.floatToIntBits(mShadowRadius);
            hashCode = 31 * hashCode + width;
            hashCode = 31 * hashCode + measureMode;
            hashCode = 31 * hashCode + Float.floatToIntBits(spacingMult);
            hashCode = 31 * hashCode + Float.floatToIntBits(spacingAdd);
            hashCode = 31 * hashCode + Float.floatToIntBits(lineHeight);
            hashCode = 31 * hashCode + (includePadding ? 1 : 0);
            hashCode = 31 * hashCode + (useLineSpacingFromFallbacks ? 1 : 0);
            hashCode = 31 * hashCode + (ellipsize != null ? ellipsize.hashCode() : 0);
            hashCode = 31 * hashCode + (singleLine ? 1 : 0);
            hashCode = 31 * hashCode + maxLines;
            hashCode = 31 * hashCode + breakStrategy;
            hashCode = 31 * hashCode + hyphenationFrequency;
            hashCode = 31 * hashCode + Arrays.hashCode(leftIndents);
            hashCode = 31 * hashCode + Arrays.hashCode(rightIndents);
            hashCode = 31 * hashCode + (text != null ? text.hashCode() : 0);
            hashCode = 31 * hashCode + (alignment != 0 ? alignment : 0);
            hashCode = 31 * hashCode + (textDirection != 0 ? textDirection : 0);
            return hashCode;
        }
    }

    // Params for the builder.

     final Params mParams = new Params();
    // Locally cached layout for an instance.
    private Layout mSavedLayout = null;

    // Text layout glyph warmer.
    private GlyphWarmer mGlyphWarmer;

    // Cache layout or not.
    private boolean mShouldCacheLayout = true;

    // Warm layout or not.
    private boolean mShouldWarmText = false;


    /**
     * Sets the intended width of the text layout.
     *
     * @param width The width of the text layout
     * @return This {@link TextLayoutBuilder} instance
     * @see #setWidth(int, int)
     */
    public TextLayoutBuilder setWidth(int width) {
        return setWidth(width, width <= 0 ? MEASURE_MODE_UNSPECIFIED : MEASURE_MODE_EXACTLY);
    }

    /**
     * Sets the intended width of the text layout while respecting the measure mode.
     *
     * @param width       The width of the text layout
     * @param measureMode The mode with which to treat the given width
     * @return This {@link TextLayoutBuilder} instance
     * @see #setWidth(int)
     */
    public TextLayoutBuilder setWidth(int width, int measureMode) {
        if (mParams.width != width || mParams.measureMode != measureMode) {
            mParams.width = width;
            mParams.measureMode = measureMode;
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Returns the text that would be packed in a layout by this TextLayoutBuilder.
     *
     * @return The text used by this TextLayoutBuilder
     */
    public String getText() {
        return mParams.text;
    }

    /**
     * Sets the text for the layout.
     *
     * @param text The text for the layout
     * @return This {@link TextLayoutBuilder} instance
     */
    public TextLayoutBuilder setText(String text) {
        if (text == mParams.text) {
            return this;
        }


        if (text != null && text.equals(mParams.text)) {
            return this;
        }

        mParams.text = text;
        mSavedLayout = null;
        return this;
    }

    /**
     * Returns the text size for this TextLayoutBuilder.
     *
     * @return The text size used by this TextLayoutBuilder
     */
    public float getTextSize() {
        return mParams.paint.getTextSize();
    }

    /**
     * Sets the text size for the layout.
     *
     * @param size The text size in pixels
     * @return This {@link TextLayoutBuilder} instance
     */

    public TextLayoutBuilder setTextSize(int size) {
        if (mParams.paint.getTextSize() != size) {
            mParams.createNewPaintIfNeeded();
            mParams.paint.setTextSize(size);
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Returns the text color for this TextLayoutBuilder.
     *
     * @return The text color used by this TextLayoutBuilder
     */

    public int getTextColor() {
        return mParams.paint.getColor().getValue();
    }

    /**
     * Sets the text color for the layout.
     *
     * @param color The text color for the layout
     * @return This {@link TextLayoutBuilder} instance
     */
    public TextLayoutBuilder setTextColor(Color color) {
        mParams.createNewPaintIfNeeded();
        mParams.color = null;
        mParams.paint.setColor(mParams.color != null ? Color.RED : color);
        mSavedLayout = null;
        return this;
    }

    /**
     * Returns the text direction for this TextLayoutBuilder.
     *
     * @return The text direction used by this TextLayoutBuilder
     */
    public int getTextDirection() {
        return mParams.textDirection;
    }


    /**
     * Sets the text direction heuristic for the layout.
     *
     * <p>TextDirectionHeuristicCompat describes how to evaluate the text of this Layout to know
     * whether to use RTL or LTR text direction
     *
     * @param textDirection The text direction heuristic for the layout
     * @return This {@link TextLayoutBuilder} instance
     */
    public TextLayoutBuilder setTextDirection(int textDirection) {
        if (mParams.textDirection != textDirection) {
            mParams.textDirection = textDirection;
            mSavedLayout = null;
        }
        return this;
    }


    /**
     * Returns the text spacing extra for this TextLayoutBuilder.
     *
     * @return The text spacing extra used by this TextLayoutBuilder
     */
    public float getTextSpacingExtra() {
        return mParams.spacingAdd;
    }


    /**
     * Returns the text spacing multiplier for this TextLayoutBuilder.
     *
     * @return The text spacing multiplier used by this TextLayoutBuilder
     */
    public float getTextSpacingMultiplier() {
        return mParams.spacingMult;
    }

    /**
     * Sets the line spacing multiplier for the layout. Line spacing multiplier will be ignored if
     * {@link TextLayoutBuilder#setLineHeight(float)} was used to set an explicit line height.
     *
     * @param spacingMultiplier the value by which each line's height is multiplied
     * @return This {@link TextLayoutBuilder} instance
     */
    public TextLayoutBuilder setTextSpacingMultiplier(float spacingMultiplier) {
        if (mParams.lineHeight == DEFAULT_LINE_HEIGHT && mParams.spacingMult != spacingMultiplier) {
            mParams.spacingMult = spacingMultiplier;
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Returns the line height for this TextLayoutBuilder.
     *
     * @return The line height used by this TextLayoutBuilder
     */
    public float getLineHeight() {
        return mParams.getLineHeight();
    }

    /**
     * Sets the line height for this layout.
     * <p>This mimics the behavior of
     * https://developer.Harmony.com/reference/Harmony/widget/TextView.html#setLineHeight(int) and
     * should not be used with {@link TextLayoutBuilder#(float)} or {@link
     * TextLayoutBuilder#setTextSpacingMultiplier(float)}.
     *
     * @param lineHeight the line height between two lines of text in px.
     * @return This {@link TextLayoutBuilder} instance.
     */
    public TextLayoutBuilder setLineHeight(float lineHeight) {
        if (mParams.lineHeight != lineHeight) {
            mParams.lineHeight = lineHeight;
            mParams.spacingAdd = lineHeight - mParams.paint.getFontMetrics().leading;
            mParams.spacingMult = 1.0f;
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Gets the text letter-space value, which determines the spacing between characters. The value
     * returned is in ems. Normally, this value is 0.0.
     *
     * @return The text letter-space value in ems.
     * @see #setLetterSpacing(float)
     */

    public float getLetterSpacing() {
        return mParams.paint.getAdvanceWidths(null, null);//原有getLetterSpacing()，等待验证

    }


    /**
     * Sets text letter-spacing in em units. Typical values for slight expansion will be around 0.05.
     * Negative values tighten text.
     *
     * @param letterSpacing A text letter-space value in ems.
     * @see #getLetterSpacing()
     */

    public TextLayoutBuilder setLetterSpacing(float letterSpacing) {
        if (getLetterSpacing() != letterSpacing) {
            mParams.createNewPaintIfNeeded();
            mParams.paint.setLetterSpacing(letterSpacing);
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Returns whether this TextLayoutBuilder should include font padding.
     *
     * @return Whether this TextLayoutBuilder should include font padding
     */
    public boolean getIncludeFontPadding() {
        return mParams.includePadding;
    }


    public TextLayoutBuilder setIncludeFontPadding(boolean shouldInclude) {
        if (mParams.includePadding != shouldInclude) {
            mParams.includePadding = shouldInclude;
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Returns the text alignment for this TextLayoutBuilder.
     *
     * @return The text alignment used by this TextLayoutBuilder
     */
    public int getAlignment() {
        return mParams.alignment;
    }

    /**
     * Sets text alignment for the layout.
     *
     * @param alignment The text alignment for the layout
     * @return This {@link TextLayoutBuilder} instance
     */

    public TextLayoutBuilder setAlignment(int alignment) {
        if (mParams.alignment != alignment) {
            mParams.alignment = alignment;
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Returns the text direction for this TextLayoutBuilder.
     *
     * @return The text direction used by this TextLayoutBuilder
     */


    /**
     * Sets the shadow layer for the layout.
     *
     * @param radius The radius of the blur for shadow
     * @param dx     The horizontal translation of the origin
     * @param dy     The vertical translation of the origin
     * @param color  The shadow color
     * @return This {@link TextLayoutBuilder} instance
     */
    public TextLayoutBuilder setShadowLayer(float radius, float dx, float dy, int color) {
        mParams.createNewPaintIfNeeded();
        mParams.mShadowRadius = radius;
        mParams.mShadowDx = dx;
        mParams.mShadowDy = dy;
        mParams.mShadowColor = color;
        BlurDrawLooper blurDrawLooper = new BlurDrawLooper(radius, dx, dy, new Color(color));
        mParams.paint.setBlurDrawLooper(blurDrawLooper);
        mSavedLayout = null;
        return this;

    }


    /**
     * Sets a text style for the layout.
     * Typeface用Font
     *
     * @param style The text style for the layout
     * @return This {@link TextLayoutBuilder} instance
     */
    public TextLayoutBuilder setTextStyle(Font style) {
        return setTypeface(style);
    }


    /**
     * Sets the typeface used by this TextLayoutBuilder.
     *
     * @param typeface The typeface for this TextLayoutBuilder
     * @return This {@link TextLayoutBuilder} instance
     */
    public TextLayoutBuilder setTypeface(Font typeface) {
        if (mParams.paint.getFont() != typeface) {
            mParams.createNewPaintIfNeeded();
            mParams.paint.setFont(typeface);
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Returns the drawable state for this TextLayoutBuilder.
     *
     * @return The drawable state used by this TextLayoutBuilder
     */

    /**
     * Updates the text colors based on the drawable state.
     * @param drawableState The current drawable state of the View holding this layout
     * @return This {@link TextLayoutBuilder} instance
     */

    /**
     * Returns the text ellipsize for this TextLayoutBuilder.
     *
     * @return The text ellipsize used by this TextLayoutBuilder
     */
    public Text.TruncationMode getEllipsize() {
        return mParams.ellipsize;
    }

    /**
     * Sets the ellipsis location for the layout.
     *
     * @param ellipsize The ellipsis location in the layout
     * @return This {@link TextLayoutBuilder} instance
     */
    public TextLayoutBuilder setEllipsize(Text.TruncationMode ellipsize) {
        if (mParams.ellipsize != ellipsize) {
            mParams.ellipsize = ellipsize;
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Set whether to respect the ascent and descent of the fallback fonts that are used in displaying
     * the text (which is needed to avoid text from consecutive lines running into each other).
     * <p>If set, fallback fonts that end up getting used can increase the ascent and descent of the
     * lines that they are used on.
     *
     * <p>This behavior is defaulted to true on API >= 28 and false otherwise.
     */

    public TextLayoutBuilder setUseLineSpacingFromFallbacks(boolean status) {
        if (mParams.useLineSpacingFromFallbacks != status) {
            mParams.useLineSpacingFromFallbacks = status;
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Returns whether to use line spacing from fallback fonts or not. See {@link
     * //   * #setUseLineSpacingFromFallbacks}
     */
    public boolean getUseLineSpacingFromFallbacks() {
        return mParams.useLineSpacingFromFallbacks;
    }

    /**
     * Returns whether the TextLayoutBuilder should show a single line.
     *
     * @return Whether the TextLayoutBuilder should show a single line or not
     */
    public boolean getSingleLine() {
        return mParams.singleLine;
    }

    /**
     * Sets whether the text should be in a single line or not.
     *
     * @param singleLine Whether the text should be in a single line or not
     * @return This {@link TextLayoutBuilder} instance
     * @see #setMaxLines(int)
     */
    public TextLayoutBuilder setSingleLine(boolean singleLine) {
        if (mParams.singleLine != singleLine) {
            mParams.singleLine = singleLine;
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Returns the number of max lines used by this TextLayoutBuilder.
     *
     * @return The number of max lines for this TextLayoutBuilder
     */
    public int getMaxLines() {
        return mParams.maxLines;
    }

    /**
     * Sets a maximum number of lines to be shown by the Layout.
     * <p>Note: Gingerbread always default to two lines max when ellipsized. This cannot be changed.
     * Use a TextView if you want more control over the number of lines.
     *
     * @param maxLines The number of maxLines to show in this Layout
     * @return This {@link TextLayoutBuilder} instance
     * @see #setSingleLine(boolean)
     */
    public TextLayoutBuilder setMaxLines(int maxLines) {
        if (mParams.maxLines != maxLines) {
            mParams.maxLines = maxLines;
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Returns the break strategy for this TextLayoutBuilder.
     *
     * @return The break strategy for this TextLayoutBuilder
     */
    public int getBreakStrategy() {
        return mParams.breakStrategy;
    }

    /**
     * Sets a break strategy breaking paragraphs into lines.
     *
     * @param breakStrategy The break strategy for breaking paragraphs into lines
     * @return This {@link TextLayoutBuilder} instance
     */
    public TextLayoutBuilder setBreakStrategy(int breakStrategy) {
        if (mParams.breakStrategy != breakStrategy) {
            mParams.breakStrategy = breakStrategy;
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Returns the hyphenation frequency for this TextLayoutBuilder.
     *
     * @return The hyphenation frequency for this TextLayoutBuilder
     */
    public int getHyphenationFrequency() {
        return mParams.hyphenationFrequency;
    }

    /**
     * Sets the frequency of automatic hyphenation to use when determining word breaks.
     *
     * @param hyphenationFrequency The frequency of automatic hyphenation to use for word breaks
     * @return This {@link TextLayoutBuilder} instance
     */
    public TextLayoutBuilder setHyphenationFrequency(int hyphenationFrequency) {
        if (mParams.hyphenationFrequency != hyphenationFrequency) {
            mParams.hyphenationFrequency = hyphenationFrequency;
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Returns the left indents set on this TextLayoutBuilder.
     *
     * @return The left indents set on this TextLayoutBuilder
     */
    public int[] getLeftIndents() {
        return mParams.leftIndents;
    }

    /**
     * Returns the right indents set on this TextLayoutBuilder.
     *
     * @return The right indents set on this TextLayoutBuilder
     */
    public int[] getRightIndents() {
        return mParams.rightIndents;
    }

    /**
     * Sets the left and right indents for this TextLayoutBuilder.
     * <p>The arrays hold an indent amount, one per line, measured in pixels. For lines past the last
     * element in the array, the last element repeats.
     *
     * @param leftIndents  The left indents for the paragraph
     * @param rightIndents The left indents for the paragraph
     * @return This {@link TextLayoutBuilder} instance
     */
    public TextLayoutBuilder setIndents(int[] leftIndents, int[] rightIndents) {
        mParams.leftIndents = leftIndents;
        mParams.rightIndents = rightIndents;
        mSavedLayout = null;
        return this;
    }

    /**
     * Returns whether the TextLayoutBuilder should cache the layout.
     *
     * @return Whether the TextLayoutBuilder should cache the layout
     */
    public boolean getShouldCacheLayout() {
        return mShouldCacheLayout;
    }

    /**
     * Returns whether the TextLayoutBuilder should cache the layout.
     *
     * @return Whether the TextLayoutBuilder should cache the layout
     */
    public TextLayoutBuilder setShouldCacheLayout(boolean shouldCacheLayout) {
        mShouldCacheLayout = shouldCacheLayout;
        return this;
    }

    /**
     * Returns whether the TextLayoutBuilder should warm the layout.
     *
     * @return Whether the TextLayoutBuilder should warm the layout
     */
    public boolean getShouldWarmText() {
        return mShouldWarmText;
    }

    /**
     * Sets whether the text should be warmed or not.
     * <p>Note: Setting this true is highly effective for large blurbs of text. This method has to be
     * called before the draw pass.
     *
     * @param shouldWarmText True to warm the text layout, false otherwise
     * @return This {@link TextLayoutBuilder} instance
     * @see #setGlyphWarmer(GlyphWarmer)
     */
    public TextLayoutBuilder setShouldWarmText(boolean shouldWarmText) {
        mShouldWarmText = shouldWarmText;
        return this;
    }

    /**
     * Returns the GlyphWarmer used by the TextLayoutBuilder.
     *
     * @return The GlyphWarmer for this TextLayoutBuilder
     */
    public GlyphWarmer getGlyphWarmer() {
        return mGlyphWarmer;
    }

    /**
     * Sets the glyph warmer to use.
     *
     * @param glyphWarmer GlyphWarmer to use to warm the text layout
     * @return This {@link TextLayoutBuilder} instance
     * @see #setShouldWarmText(boolean)
     */
    public TextLayoutBuilder setGlyphWarmer(GlyphWarmer glyphWarmer) {
        mGlyphWarmer = glyphWarmer;
        return this;
    }

    /**
     * Returns the min width expressed in ems.
     *
     * @return the min width expressed in ems or -1
     * @see #setMinEms(int)
     */
    public int getMinEms() {
        return mMinWidthMode == EMS ? mMinWidth : -1;
    }

    /**
     * Sets the min width expressed in ems.
     *
     * @param minEms min width expressed in ems
     * @return This {@link TextLayoutBuilder} instance
     * @see #setMaxEms(int)
     * @see #setMinWidth(int)
     */
    public TextLayoutBuilder setMinEms(int minEms) {
        mMinWidth = minEms;
        mMinWidthMode = EMS;
        return this;
    }

    /**
     * Returns the min width expressed in pixels.
     *
     * @return the min width expressed in pixels or -1, if the min width was set in ems instead
     * @see #setMinWidth(int)
     */

    public int getMinWidth() {
        return mMinWidthMode == PIXELS ? mMinWidth : -1;
    }

    /**
     * Sets the min width expressed in pixels.
     *
     * @param minWidth min width expressed in pixels.
     * @return This {@link TextLayoutBuilder} instance
     * @see #setMaxWidth(int)
     * @see #setMinEms(int)
     */
    public TextLayoutBuilder setMinWidth(int minWidth) {
        mMinWidth = minWidth;
        mMinWidthMode = PIXELS;
        return this;
    }

    /**
     * Returns the max width expressed in ems.
     *
     * @return the max width expressed in ems or -1, if max width is set in pixels instead
     * @see #setMaxEms(int)
     */
    public int getMaxEms() {
        return mMaxWidthMode == EMS ? mMaxWidth : -1;
    }

    /**
     * Sets the max width expressed in ems.
     *
     * @param maxEms max width expressed in ems
     * @return This {@link TextLayoutBuilder} instance
     * @see #setMaxWidth(int)
     * @see #setMinEms(int)
     */
    public TextLayoutBuilder setMaxEms(int maxEms) {
        mMaxWidth = maxEms;
        mMaxWidthMode = EMS;
        return this;
    }

    /**
     * Returns the max width expressed in pixels.
     *
     * @return the max width expressed in pixels or -1, if the max width was set in ems instead
     * @see #setMaxWidth(int)
     */

    public int getMaxWidth() {
        return mMaxWidthMode == PIXELS ? mMaxWidth : -1;
    }

    /**
     * Sets the max width expressed in pixels.
     *
     * @param maxWidth max width expressed in pixels
     * @return This {@link TextLayoutBuilder} instance
     * @see #setMaxEms(int)
     * @see #setMinWidth(int)
     */
    public TextLayoutBuilder setMaxWidth(int maxWidth) {
        mMaxWidth = maxWidth;
        mMaxWidthMode = PIXELS;
        return this;
    }


    /**
     * Sets the density of this layout. This should typically be set to your current display's density
     *
     * @return This {@link TextLayoutBuilder}
     */


    /**
     * @return The justification mode of this layout.
     */

    public int getJustificationMode() {
        return mParams.justificationMode;
    }

    /**
     * Set justification mode. The default value is JUSTIFICATION_MODE_NONE. If the last line is too
     * short for justification, the last line will be displayed with the alignment
     *
     * @param justificationMode The justification mode to use
     * @return This {@link TextLayoutBuilder} instance
     */

    public TextLayoutBuilder setJustificationMode(int justificationMode) {
        if (mParams.justificationMode != justificationMode) {
            mParams.justificationMode = justificationMode;
            mSavedLayout = null;
        }
        return this;
    }

    /**
     * Sets whether zero-length text should be laid out or not.
     *
     * @param shouldLayoutZeroLengthText True to lay out zero-length text, false otherwise.
     * @return This {@link TextLayoutBuilder} instance
     */
    public TextLayoutBuilder setShouldLayoutZeroLengthText(boolean shouldLayoutZeroLengthText) {
        if (mParams.shouldLayoutZeroLengthText != shouldLayoutZeroLengthText) {
            mParams.shouldLayoutZeroLengthText = shouldLayoutZeroLengthText;
            if (mParams.text.length() == 0) {
                mSavedLayout = null;
            }
        }

        return this;
    }

    /**
     * Builds and returns a {@link Layout}.
     *
     * @return A {@link Layout} based on the parameters set. Returns null if no text was specified and
     * empty text is not explicitly allowed (see {@link #setShouldLayoutZeroLengthText(boolean)}).
     */

    public Layout build() {
        sCache  = new LruBuffer<>(100);
        // Return the cached layout if no property changed.
        if (mShouldCacheLayout && mSavedLayout != null) {
            return mSavedLayout;
        }

        if (mParams.text == null
                || (mParams.text.length() == 0 && !mParams.shouldLayoutZeroLengthText)) {
            return null;
        }

        boolean hasClickableSpans = false;
        int hashCode = -1;


        // If the text has ClickableSpans, it will be bound to different
        // click listeners each time. It is unsafe to cache these text Layouts.
        // Hence they will not be in cache.
        if (mShouldCacheLayout && !hasClickableSpans) {
            hashCode = mParams.hashCode();
            Layout cachedLayout = sCache.get(hashCode);
            if (cachedLayout != null) {
                return cachedLayout;
            }
        }

//    BoringLayout.Metrics metrics = null;
        BoringLayout.Metrics metrics = null;
        int numLines = mParams.singleLine ? 1 : mParams.maxLines;

        // Try creating a boring layout only if singleLine is requested.
        if (numLines == 1) {
            try {

//        metrics = BoringLayout.isBoring(mParams.text, mParams.paint);
            } catch (NullPointerException e) {
                // On older Samsung devices (< M), we sometimes run into a NPE here where a FontMetricsInt
                // object created within BoringLayout is not properly null-checked within TextLine.
                // Its ok to ignore this exception since we'll create a regular StaticLayout later.

            }
        }


        // If we used a large static value it would break RTL due to drawing text at the very end of the
        // large value.

        int width;

        Rect rect = new Rect();
        switch (mParams.measureMode) {
            case MEASURE_MODE_UNSPECIFIED:

                RichTextBuilder richTextBuilder = new RichTextBuilder();
                RichText richText = richTextBuilder.addText(mParams.text).build();
                width = (int) Math.ceil(new RichTextLayout(richText, mParams.paint, rect, mParams.width).calculateTextWidth(richText));

                break;
            case MEASURE_MODE_EXACTLY:
                width = mParams.width;
                break;
            case MEASURE_MODE_AT_MOST:
                RichTextBuilder richTextBuilderMin = new RichTextBuilder();
                RichText richTextMin = richTextBuilderMin.addText(mParams.text).build();
                width = Math.min((int) Math.ceil(new RichTextLayout(richTextMin, mParams.paint, rect, mParams.width).calculateTextWidth(richTextMin)), mParams.width);
                break;
            default:
                throw new IllegalStateException("Unexpected measure mode " + mParams.measureMode);
        }

        final int lineHeight = mParams.getLineHeight();
        if (mMaxWidthMode == EMS) {
            width = Math.min(width, mMaxWidth * lineHeight);
        } else {
            width = Math.min(width, mMaxWidth);

        }

        if (mMinWidthMode == EMS) {
            width = Math.max(width, mMinWidth * lineHeight);
        } else {
            width = Math.max(width, mMinWidth);
        }


        Layout layout;
        if (metrics != null) {
            layout = BoringLayout.make(
                    mParams.text,
                    mParams.paint,
                    width,
                    mParams.alignment,
                    metrics,
                    mParams.includePadding,
                    mParams.ellipsize,
                    width,
                    mParams.isRtl,
                    rect,
                    mParams.text.length()

            );
        } else {
            while (true) {
                try {
                    layout = StaticLayoutHelper.make(mParams.text,
                            mParams.paint,
                            width,
                            rect, context,
                            mParams.alignment,
                            mParams.includePadding,
                            mParams.leftIndents,
                            mParams.rightIndents,
                            mParams.textDirection,
                            mParams.breakStrategy,
                            mParams.hyphenationFrequency
                    );
                } catch (IndexOutOfBoundsException e) {
                    // Workaround for https://code.google.com/p/Harmony/issues/detail?id=35412
                    if (!(mParams.text instanceof String)) {
                        // remove all Spannables and re-try
                        mParams.text = mParams.text.toString();
                        continue;
                    } else {
                        // If it still happens with all Spannables removed we'll bubble the exception up
                        throw e;
                    }
                }

                break;
            }
        }

        // Do not cache if the text has ClickableSpans.
        if (mShouldCacheLayout && !hasClickableSpans) {
            mSavedLayout = layout;
            sCache.put(hashCode, layout);
        }

        // Force a new paint.
        mParams.mForceNewPaint = true;

        if (mShouldWarmText && mGlyphWarmer != null) {
            // Draw the text in a background thread to warm the cache.
            mGlyphWarmer.warmLayout(layout);
        }

        return layout;
    }
}
