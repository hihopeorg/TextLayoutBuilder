/*
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fbui.textlayoutbuilder.util;

import ohos.agp.components.Text;
import ohos.agp.render.Paint;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Rect;

public class BoringLayout extends MySimpleTextLayout {
    private String mDirect;
    private Paint mPaint;
    public BoringLayout(String textSource, Paint textPaint, Rect rect, int width, boolean isRtl, Text.TruncationMode ellipsize, int maxWidth) throws NullPointerException {
        super(textSource, textPaint, rect, width, isRtl);
    }

    public static BoringLayout make(CharSequence source, Paint paint, int outerWidth,
                                    int align, Metrics metrics,
                                    boolean includePad, Text.TruncationMode ellipsize, int ellipsizedWidth, boolean isRtl, Rect rect, int maxWidth){
        return new  BoringLayout((String) source, paint, rect, outerWidth, isRtl, ellipsize,maxWidth);
    }

    public static class Metrics extends Paint.FontMetrics {
        public int width;

        public Metrics(float top, float ascent, float descent, float bottom, float leading) {
            super(top, ascent, descent, bottom, leading);
        }

        @Override public String toString() {
            return super.toString() + " width=" + width;
        }


    }

}
