/*
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fbui.textlayoutbuilder;


import com.facebook.fbui.textlayoutbuilder.util.MySimpleTextLayout;
import ohos.agp.render.Paint;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

/* package */ class StaticLayoutHelper {

  private static final String SPACE_AND_ELLIPSIS = " \u2026";

  public static MySimpleTextLayout make(
          String text,
          Paint paint,
          int width,
          Rect rect,
          Context context,
          int alignment,
          boolean includePadding,
          int[] leftIndents,
          int[] rightIndents,
          int textDirection,
          int breakStrategy,
          int hyphenationFrequency

  ) {
    MySimpleTextLayout builder= new MySimpleTextLayout(text,paint,rect,width,includePadding);
      return builder;
    }

  private static void swap(int[] array, int i, int j) {
    int tmp = array[i];
    array[i] = array[j];
    array[j] = tmp;
  }
}
